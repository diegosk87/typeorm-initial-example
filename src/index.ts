import 'reflect-metadata';

import * as express from 'express'; 
import { createConnection } from 'typeorm';
import personRoutes from './routes/test-routes';

const app = express();

createConnection().then(connect => {
    app.listen(3000, () => console.log("Server on port 3000"));
});

app.use(express.json());

app.use(personRoutes);