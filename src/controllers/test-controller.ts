import { Request, Response } from 'express';
import { getRepository } from "typeorm";

import { Test } from "../models/test";

export const controller = {
    index: (req:Request, res:Response):void => {
        getRepository(Test).find()
            .then(tests => {
                res.status(200).send(tests);
            }).catch(error => {
                res.status(500).send(error);
            });
    },

    show: (req:Request, res:Response):void => {
        getRepository(Test).findOne(req.params.id)
            .then(test => {
                res.status(200).send(test);
            }).catch(error => {
                res.status(500).send(error);
            });
    },

    store: (req:Request, res:Response):void => {    
        getRepository(Test).save(req.body)
            .then(result => res.status(200).send(result))
            .catch(error => res.status(500).send(error));
    },

    update: (req:Request, res:Response):void => { 
        let repository = getRepository(Test);

        repository.findOne(req.params.id)
            .then(test => {
                if(test) {
                    repository.merge(test, req.body);
                    repository.save(test)
                        .then(result => res.status(200).send(result))
                        .catch(error => res.status(500).send(error));
                }
                else res.status(500).send({message: 'ID not fond'});
            })
            .catch(error => res.status(500).send(error)); 
    },
    
    delete: (req:Request, res:Response):void => {
        getRepository(Test).delete(req.params.id)
            .then(result => res.status(200).send(result))
            .catch(error => res.status(500).send(error));
    }
};