import { Router } from 'express';
import { controller } from '../controllers/test-controller';

const router = Router();

router.get('/test', controller.index);
router.get('/test/:id', controller.show);
router.post('/test', controller.store);
router.put('/test/:id', controller.update);
router.delete('/test/:id', controller.delete);

export default router;