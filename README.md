# TypeORM Initial API REST

This project aims to offer a starting point in this ORM called TypeORM to create a REST API

## Starting 🚀

Just do a pull to the repository.

### Pre-requirements 📋

You just need to have installed

* [NodeJS](https://nodejs.org/en/) - The JS Framework
* An code editor, i recommended [Visual Studio Code](https://code.visualstudio.com/)

## Executing tests ⚙️

Only change the route of your .ts on ormconfig.json and then execute this command 

```
npm test
```

## Author

* **Diego Noveron** - *Developer* - [diegosk87](https://github.com/diegosk87)

## Licencia 📄

This proyect is under license (GPL) - see the file [LICENSE.md](LICENSE.md) for more details.

---
by [diegosk87](https://github.com/diegosk87)�